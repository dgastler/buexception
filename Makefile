SHELL = bash
BUILD_MODE = x86

#x86 linux
CXX=g++
RUNTIME_LDPATH?=${PWD}

export

#Exception class
LIBRARY_OUTPUT_PATH ?= lib
LIBRARY_EXCEPTION = ${LIBRARY_OUTPUT_PATH}/libToolException.so
LIBRARY_EXCEPTION_SOURCES  = $(wildcard src/BUException/*.cc)
LIBRARY_EXCEPTION_OBJECT_FILES = $(patsubst src/%.cc,obj/%.o,${LIBRARY_EXCEPTION_SOURCES})


INCLUDE_PATH = \
							-Iinclude  

LIBRARY_PATH = \
							-Llib 

ifdef BOOST_INC
INCLUDE_PATH +=-I$(BOOST_INC)
endif
ifdef BOOST_LIB
LIBRARY_PATH +=-L$(BOOST_LIB)
endif

INSTALL_PATH ?= ./install


CXX_FLAGS = -g -O3 -rdynamic -Wall -MMD -MP -fPIC ${INCLUDE_PATH} -Werror -Wno-literal-suffix

CXX_FLAGS += -std=c++11 -fno-omit-frame-pointer -pedantic -Wno-ignored-qualifiers -Werror=return-type -Wextra -Wno-long-long -Winit-self -Wno-unused-local-typedefs  -Woverloaded-virtual  ${COMPILETIME_ROOT} ${COMPILETIME_ROOT} ${FALLTHROUGH_FLAGS} -Wno-unused-result

ifdef MAP_TYPE
CXX_FLAGS += ${MAP_TYPE}
endif

LINK_LIBRARY_FLAGS = -shared -fPIC -Wall -Wl,--no-as-needed -g -O3 -rdynamic -Wl,-rpath=${RUNTIME_LDPATH}/lib ${COMPILETIME_ROOT}

.PHONY: all _all clean _cleanall build _buildall self plugin 

default: build
clean: _cleanall
_cleanall:
	rm -rf obj
	rm -rf bin
	rm -rf lib

all: _all
build: _all
buildall: _all
_all: self
self: ${LIBRARY_EXCEPTION}

# ------------------------
# exception library
# ------------------------
${LIBRARY_EXCEPTION}: ${LIBRARY_EXCEPTION_OBJECT_FILES}
	mkdir -p $(dir $@)
	${CXX} -shared -fPIC -Wall -g -O3 -rdynamic ${LIBRARY_EXCEPTION_OBJECT_FILES} -o $@

${LIBRARY_EXCEPTION_OBJECT_FILES}: obj/%.o : src/%.cc 
	mkdir -p $(dir $@)
	${CXX} ${CXX_FLAGS} -c $< -o $@



# -----------------------
# install
# -----------------------
install: all
	 install -m 775 -d ${INSTALL_PATH}/lib
	 install -b -m 775 ./lib/* ${INSTALL_PATH}/lib
	 install -m 775 -d ${INSTALL_PATH}/include/BUException
	 cp -r ./include/BUException/* ${INSTALL_PATH}/include/BUException/



